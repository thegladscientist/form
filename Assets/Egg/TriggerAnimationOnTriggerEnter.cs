﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerAnimationOnTriggerEnter : MonoBehaviour {
    private Animator animator;

	// Use this for initialization
	void Start () {
        animator = GetComponent<Animator>();
        //animator.Play("time1");
    }
	
	// Update is called once per frame
	void Update () {

	}
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == 10)
            animator.Play("time1");

    }
}
